// Copyright (C) 2022,  Asymptotic Inc.
//      Author: Taruntej Kanakamalla <taruntej@asymptotic.io>
//
// This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
// If a copy of the MPL was not distributed with this file, You can obtain one at
// <https://mozilla.org/MPL/2.0/>.
//
// SPDX-License-Identifier: MPL-2.0

use gst::glib;
use gst::prelude::*;
use gst::subclass::prelude::*;
use gst::StructureRef;
use gst_sdp::*;
use gst_webrtc::*;
use parse_link_header;
use reqwest::StatusCode;

use once_cell::sync::Lazy;
use reqwest::header::HeaderMap;
use reqwest::header::HeaderValue;
use reqwest::redirect::Policy;
use std::sync::Mutex;

static CAT: Lazy<gst::DebugCategory> = Lazy::new(|| {
    gst::DebugCategory::new("whipsink", gst::DebugColorFlags::empty(), Some("Whip Sink"))
});

const MAX_REDIRECTS: u8 = 10;

#[derive(Debug, Clone)]
struct Settings {
    whip_endpoint: Option<String>,
    use_link_headers: bool,
    whip_resource_url: Option<String>,
    auth_token: Option<String>,
    options_redirects: u8,
    post_redirects: u8,
}

#[allow(clippy::derivable_impls)]
impl Default for Settings {
    fn default() -> Self {
        Self {
            whip_endpoint: None,
            use_link_headers: false,
            whip_resource_url: None,
            auth_token: None,
            options_redirects: 0,
            post_redirects: 0,
        }
    }
}

pub struct WhipSink {
    settings: Mutex<Settings>,
    webrtcbin: gst::Element,
}

impl Default for WhipSink {
    fn default() -> Self {
        let webrtcbin = gst::ElementFactory::make("webrtcbin", Some("whip-webrtcbin"))
            .expect("Failed to create webrtcbin");
        Self {
            settings: Mutex::new(Settings::default()),
            webrtcbin,
        }
    }
}

impl BinImpl for WhipSink {}

impl GstObjectImpl for WhipSink {}

impl ElementImpl for WhipSink {
    fn metadata() -> Option<&'static gst::subclass::ElementMetadata> {
        static ELEMENT_METADATA: Lazy<gst::subclass::ElementMetadata> = Lazy::new(|| {
            gst::subclass::ElementMetadata::new(
                "WHIP Sink Bin",
                "Sink/Network/WebRTC",
                "A bin to stream media using the WebRTC HTTP Ingestion Protocol (WHIP)",
                "Taruntej Kanakamalla <taruntej@asymptotic.io>",
            )
        });
        Some(&*ELEMENT_METADATA)
    }

    fn pad_templates() -> &'static [gst::PadTemplate] {
        static PAD_TEMPLATES: Lazy<Vec<gst::PadTemplate>> = Lazy::new(|| {
            let sink_caps = gst::Caps::builder("application/x-rtp").build();
            let sink_pad_template = gst::PadTemplate::new(
                "sink_%u",
                gst::PadDirection::Sink,
                gst::PadPresence::Request,
                &sink_caps,
            )
            .unwrap();

            vec![sink_pad_template]
        });

        PAD_TEMPLATES.as_ref()
    }

    fn request_new_pad(
        &self,
        element: &Self::Type,
        _templ: &gst::PadTemplate,
        _name: Option<String>,
        _caps: Option<&gst::Caps>,
    ) -> Option<gst::Pad> {
        let wb_sink_pad = self.webrtcbin.request_pad_simple("sink_%u").unwrap();
        let sink_pad = gst::GhostPad::new(Some(&wb_sink_pad.name()), gst::PadDirection::Sink);
        sink_pad.set_target(Some(&wb_sink_pad)).unwrap();
        element.add_pad(&sink_pad).unwrap();
        Some(sink_pad.upcast())
    }

    fn change_state(
        &self,
        element: &Self::Type,
        transition: gst::StateChange,
    ) -> Result<gst::StateChangeSuccess, gst::StateChangeError> {
        match self.parent_change_state(element, transition) {
            Ok(s) => {
                #[allow(clippy::single_match)]
                match transition {
                    gst::StateChange::PausedToReady => {
                        self.terminate_session();
                    }
                    _ => {}
                };
                Ok(s)
            }
            Err(e) => Err(e),
        }
    }
}

impl ObjectImpl for WhipSink {
    fn properties() -> &'static [glib::ParamSpec] {
        static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
            vec![glib::ParamSpecString::new(
                    "whip-endpoint",
                    "WHIP Endpoint",
                    "The WHIP server endpoint to POST SDP offer. \
                    e.g.: https://example.com/whip/endpoint/room1234",
                    None,
                    glib::ParamFlags::READWRITE | gst::PARAM_FLAG_MUTABLE_READY,
                ),

                glib::ParamSpecBoolean::new(
                    "use-link-headers",
                    "Use Link Headers",
                    "Use Link Headers to configure ice-servers present in the WHIP server response to the POST or OPTIONS request. \
                    If set to TRUE and the WHIP server returns valid ice-servers, \
                    this property overrides the ice-servers values set using the stun-server and turn-server properties.",
                    false,
                    glib::ParamFlags::READWRITE | gst::PARAM_FLAG_MUTABLE_READY,
                ),

                glib::ParamSpecString::new(
                    "auth-token",
                    "Authorization Token",
                    "Authentication Bearer token to use, will be sent in the HTTP Header as 'Bearer <auth-token>",
                    None,
                    glib::ParamFlags::READWRITE | gst::PARAM_FLAG_MUTABLE_READY,
                ),
            ]
        });
        PROPERTIES.as_ref()
    }

    fn set_property(
        &self,
        _obj: &Self::Type,
        _id: usize,
        value: &glib::Value,
        pspec: &glib::ParamSpec,
    ) {
        match pspec.name() {
            "whip-endpoint" => {
                let mut settings = self.settings.lock().unwrap();
                settings.whip_endpoint = value.get().expect("Failed to read whip endpint");
            }
            "use-link-headers" => {
                let mut settings = self.settings.lock().unwrap();
                settings.use_link_headers = value.get().expect("Failed to read use-link-headers");
            }
            "auth-token" => {
                let mut settings = self.settings.lock().unwrap();
                settings.auth_token = value.get().expect("Failed to get auth-token");
            }
            _ => unimplemented!(),
        }
    }

    fn property(&self, _obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
        match pspec.name() {
            "whip-endpoint" => {
                let settings = self.settings.lock().unwrap();
                settings.whip_endpoint.to_value()
            }
            "use-link-headers" => {
                let settings = self.settings.lock().unwrap();
                settings.use_link_headers.to_value()
            }
            "auth-token" => {
                let settings = self.settings.lock().unwrap();
                settings.auth_token.to_value()
            }
            _ => unimplemented!(),
        }
    }

    fn constructed(&self, obj: &Self::Type) {
        self.parent_constructed(obj);

        obj.add(&self.webrtcbin).unwrap();
        self.webrtcbin
            .set_property("bundle-policy", gst_webrtc::WebRTCBundlePolicy::MaxBundle);

        let ele_weak = obj.downgrade();
        self.webrtcbin.connect("on-negotiation-needed", false, {
            move |_args| {
                let ele = match ele_weak.upgrade() {
                    Some(e) => e,
                    None => return None,
                };

                let whipsink = ele.imp();
                whipsink.configure_ice_servers(None);

                let ele_weak = ele.downgrade();
                /*
                 * Promise for 'create-offer' signal emitted to webrtcbin
                 * Closure is called when the promise is fulfilled
                 */
                let promise = gst::Promise::with_change_func(move |reply| {
                    let ele = match ele_weak.upgrade() {
                        Some(ele) => ele,
                        None => return,
                    };
                    let whipsink = ele.imp();
                    let offer_sdp = match reply {
                        Ok(Some(sdp)) => sdp,
                        Ok(None) => {
                            gst::error!(CAT, "create-offer::Promise returned with No reply");
                            return;
                        }
                        Err(e) => {
                            gst::error!(CAT, "create-offer::Promise returned with error {:?}", e);
                            return;
                        }
                    };
                    whipsink.set_sdp(offer_sdp);
                });
                whipsink
                    .webrtcbin
                    .emit_by_name::<()>("create-offer", &[&None::<gst::Structure>, &promise]);
                None
            }
        });

        self.webrtcbin.connect("on-new-transceiver", false, {
            move |args| {
                let trans = args[1].get::<gst_webrtc::WebRTCRTPTransceiver>().unwrap();
                trans.set_direction(gst_webrtc::WebRTCRTPTransceiverDirection::Sendonly);

                None
            }
        });
    }

    fn dispose(&self, _obj: &Self::Type) {}
}

/*
 * Using the Object_Subclass trait
 * define the name, type and parenttype of the element
 */
#[glib::object_subclass]
impl ObjectSubclass for WhipSink {
    const NAME: &'static str = "WhipSink";
    type Type = super::WhipSink;
    type ParentType = gst::Bin;

    fn with_class(_klass: &Self::Class) -> Self {
        WhipSink::default()
    }
}
impl WhipSink {
    fn configure_ice_servers(&self, endpoint_opt: Option<String>) {
        let whipsink: &WhipSink = self;
        let mut settings = whipsink.settings.lock().unwrap();
        if settings.use_link_headers {
            let pol = reqwest::redirect::Policy::none();
            let client = build_reqwest_client(pol);
            let endpoint = match endpoint_opt {
                Some(e) => Some(e),
                None => settings.whip_endpoint.clone(),
            };

            let mut headermap = HeaderMap::new();
            match &settings.auth_token {
                Some(token) => {
                    let bearer_token = "Bearer ".to_owned() + token.as_str();
                    headermap.insert(
                        reqwest::header::AUTHORIZATION,
                        HeaderValue::from_str(bearer_token.as_str())
                            .expect("Failed to set auth token to header"),
                    );
                }
                None => {}
            }
            //todo : convert this async
            let resp = client
                .request(reqwest::Method::OPTIONS, endpoint.as_ref().unwrap())
                .headers(headermap)
                .send();
            let resp = match resp {
                Ok(r) => r,
                Err(e) => {
                    gst::error!(CAT, "Failed to send request {:?}", e);
                    return;
                }
            };
            match resp.status() {
                StatusCode::NO_CONTENT => {
                    let headermap = resp.headers();
                    self.update_ice_servers(headermap);
                }
                status if status.is_redirection()  => {
                    if settings.options_redirects < MAX_REDIRECTS {
                        settings.options_redirects += 1;
                        let redirect = parse_redirect_location(resp.headers(), endpoint);
                        gst::info!(CAT, "Redirecting endpoint to {:?}", redirect);
                        drop(settings);
                        self.configure_ice_servers(redirect);
                    } else {
                        gst::error!(
                            CAT,
                            "Too many redirects. Unable to connect to do OPTIONS request"
                        );
                    }
                }
                status => {
                    gst::error!(
                        CAT,
                        "configure_ice_servers: Unexpected response {} {:?} from {:?}",
                        status,
                        resp.bytes().unwrap(),
                        settings.whip_endpoint
                    )
                }
            }
        }
    }


    fn update_ice_servers(&self, headermap: &HeaderMap) {
        let whipsink: &WhipSink = self;
        let links = headermap.get_all("link");
        for link in links.iter() {
            let link = link.to_str().expect("String conv failed");
            /*
             * Note: The Demo WHIP Server appends an extra ; at the end of each link
             * but not needed as per https://datatracker.ietf.org/doc/html/rfc8288#section-3.5
             */
            let link = link.trim_matches(';');
            let item_map = parse_link_header::parse_with_rel(link);
            if let Err(e) = item_map {
                gst::error!(CAT, "configure_ice_servers {} - Error {:?}", link, e);
                return;
            }

            let item_map = item_map.unwrap();
            if let Some(link) = item_map.get("ice-server") {
                // webrtcbin needs ice servers in format '<scheme>://<user:pass>@<url>'
                let mut ice_svr_url;

                // check if uri has ://
                if link.uri.has_authority() {
                    // use raw_uri as is
                    ice_svr_url = link.raw_uri.as_str().to_string();
                } else {
                    // construct url as '<scheme>://<user:pass>@<url>'
                    ice_svr_url = format!("{}://",link.uri.scheme());
                    if let Some(user) = link.params.get("username") {
                        ice_svr_url += user.as_str();
                        if let Some(pass) = link.params.get("credential") {
                            ice_svr_url = ice_svr_url + ":" + pass.as_str();
                        }
                        ice_svr_url += "@";
                    }
                    ice_svr_url += link.raw_uri.strip_prefix((link.uri.scheme().to_owned()+":").as_str()).expect("strip 'scheme:' from raw uri");
                }
                // set ice servers to webrtcbin
                #[allow(clippy::collapsible_if)]
                if link.raw_uri.starts_with("stun") {
                    gst::debug!(CAT, "Setting stun server {}", ice_svr_url);
                    whipsink
                        .webrtcbin
                        .set_property_from_str("stun-server", ice_svr_url.as_str());
                } else if link.raw_uri.starts_with("turn") {
                    gst::debug!(CAT, "Setting turn server {}", ice_svr_url);
                    if !whipsink
                        .webrtcbin
                        .emit_by_name::<bool>("add-turn-server", &[&ice_svr_url.as_str()])
                    {
                        gst::error!(CAT, "Falied to set turn server {}", ice_svr_url);
                    }
                }
            }
        }
    }

    fn set_sdp(&self, offer_sdp: &StructureRef) {
        let whipsink: &WhipSink = self;
        let sendval = offer_sdp.value("offer").expect("set_sdp: Failed to get sendval");

        let offer = sendval.get::<gst_webrtc::WebRTCSessionDescription>().expect("Failed to get offer");
        whipsink.webrtcbin.emit_by_name::<()>(
            "set-local-description",
            &[&offer, &None::<gst::Promise>],
        );

        let res = self.exchange_sdp(offer, None);

        let answer = match res {
            Ok(answer) => answer,
            Err(e) => {
                gst::error!(CAT, "Failed to exchange sdp: {}", e);
                return;
            }
        };

        whipsink.webrtcbin.emit_by_name::<()>(
            "set-remote-description",
            &[&answer, &None::<gst::Promise>],
        );
    }

    fn exchange_sdp(
        &self,
        offer: gst_webrtc::WebRTCSessionDescription,
        endpoint_opt: Option<String>,
    ) -> Result<gst_webrtc::WebRTCSessionDescription, String> {
        let whipsink: &WhipSink = self;
        let mut settings = whipsink.settings.lock().unwrap();

        /*
         * Default policy for redirect does not share the auth token to new location
         * So disable inbuilt redirecting and
         * do a recursive call upon 3xx response code
         */
        let pol = reqwest::redirect::Policy::none();
        let client = build_reqwest_client(pol);

        let sdp = offer.sdp();
        let body = sdp.as_text().unwrap();

        let endpoint = match endpoint_opt {
            Some(e) => Some(e),
            None => settings.whip_endpoint.clone(),
        };

        gst::info!(CAT, "using endpoint {:?}", endpoint);
        let mut headermap = HeaderMap::new();
        headermap.insert(
            reqwest::header::CONTENT_TYPE,
            HeaderValue::from_static("application/sdp"),
        );

        match &settings.auth_token {
            Some(token) => {
                let bearer_token = "Bearer ".to_owned() + token.as_str();
                headermap.insert(
                    reqwest::header::AUTHORIZATION,
                    HeaderValue::from_str(bearer_token.as_str())
                        .expect("Failed to set auth token to header"),
                );
            }
            None => {}
        }

        let resp = client
            .request(reqwest::Method::POST, endpoint.as_ref().unwrap())
            .headers(headermap)
            .body(body)
            .send();
        let mut resp = match resp {
            Ok(r) => r,
            Err(e) => {
                return Err(format!("Failed to send POST request {:?}", e));
            }
        };

        let res = match resp.status() {
            StatusCode::OK | StatusCode::CREATED => {
                let mut ans_bytes: Vec<u8> = vec![];
                match resp.copy_to(&mut ans_bytes) {
                    Ok(_r) => {}
                    Err(e) => {
                        gst::error!(CAT, "Failed to copy the answer to bytes {}", e)
                    }
                }

                let ans_sdp = sdp_message::SDPMessage::parse_buffer(&ans_bytes).unwrap();
                let answer = gst_webrtc::WebRTCSessionDescription::new(
                    gst_webrtc::WebRTCSDPType::Answer,
                    ans_sdp,
                );

                /*
                 * Note: Not taking care of 'Link' headers in POST response
                 * because we do a mandatory OPTIONS request before 'create-offer'
                 * and update the ICE servers from response to OPTIONS
                 */
                let location = resp.headers().get(reqwest::header::LOCATION).unwrap();
                let mut url = reqwest::Url::parse(endpoint.as_ref().unwrap().as_str()).unwrap();
                url.set_path(location.to_str().unwrap());
                settings.whip_resource_url = Some(url.to_string());
                Ok(answer)
            }

            s => {
                if s.is_redirection() {
                    if settings.post_redirects < MAX_REDIRECTS {
                        settings.post_redirects += 1;
                        let redirect = parse_redirect_location(resp.headers(), endpoint);
                        gst::info!(CAT, "Redirecting endpoint to {:?}", redirect);
                        drop(settings);
                        self.exchange_sdp(offer, redirect)
                    } else {
                        Err(("Too many redirects. Unable to connect to do POST").to_string())
                    }
                } else if s.is_server_error() {
                    //Check and handle 'Retry-After' header in case of server error
                    todo!()
                } else {
                    Err(format!(
                        "Unexpected response {:?} {:?}",
                        s,
                        resp.bytes().unwrap()
                    ))
                }
            }
        };
        res
    }

    fn terminate_session(&self) {
        let whipsink: &WhipSink = self;
        let settings = whipsink.settings.lock().unwrap();
        let resource_url = match &settings.whip_resource_url {
            Some(u) => u,
            None => {
                gst::error!(CAT, "No string in 'settings.whip_resource_url'");
                return;
            }
        };

        let mut headermap = HeaderMap::new();
        match &settings.auth_token {
            Some(token) => {
                let bearer_token = "Bearer ".to_owned() + token.as_str();
                headermap.insert(
                    reqwest::header::AUTHORIZATION,
                    HeaderValue::from_str(bearer_token.as_str())
                        .expect("Failed to set auth token to header"),
                );
            }
            None => {}
        }

        gst::debug!(CAT, "DELETE request on {}", resource_url);
        let client = build_reqwest_client(
            reqwest::redirect::Policy::default(),
        );
        let resp = client
            .delete(resource_url.as_str())
            .headers(headermap)
            .send()
            .unwrap();
        gst::debug!(CAT, "response to DELETE : {}", resp.status());
    }
}

fn parse_redirect_location(headermap: &HeaderMap, endpoint: Option<String>) -> Option<String> {
    let location = headermap.get(reqwest::header::LOCATION).unwrap();
    if location.as_bytes().starts_with(b"http") {
        Some(location.to_str().unwrap().to_string())
    } else {
        let mut url = reqwest::Url::parse(endpoint.as_ref().unwrap().as_str()).unwrap();
        url.set_path(location.to_str().unwrap());
        Some(url.to_string())
    }
}

fn build_reqwest_client(pol: Policy) -> reqwest::blocking::Client {
    let client_builder = reqwest::blocking::Client::builder();
    client_builder.redirect(pol).build().unwrap()
}
